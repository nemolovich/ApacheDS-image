# Apache-DS OpenLDAP server

## Releases
The available images can be found on [DockerHub/nemolovich/apacheDS](https://hub.docker.com/repository/docker/nemolovich/apacheds/tags?page=1&ordering=last_updated).


## Build Image
Build image from:
```sh
docker build -t nemolovich/apacheds ./
```

## Run Container
Run image with:
```sh
docker run --name apacheds -d -p 10389:10389 -e DOMAIN_SUFFIX="dc=mydomain,dc=com" -e LDAP_ADMIN_PASSWORD="MyPaSSw0rd" -v /root/volumes/custom:/root/custom:Z -v /root/volumes/logs:/root/logs:Z nemolovich/apacheds
```


### --env|-e

| **Environment Variable Name** | **Default Value**     | **Description**                                                    |
|------------------------------:|:----------------------|:-------------------------------------------------------------------|
| **DOMAIN_SUFFIX**             | "_dc=example,dc=com_" | The domain name for LDAP                                           |
| **LDAP_ADMIN_PASSWORD**       | "_secret_"            | The LDAP admin password                                            |
| **USERS_OU**                  | "_ou=users_"          | The users Organizational Unit value                                |
| **GROUPS_OU**                 | "_ou=groups_"         | The groups Organizational Unit value                               |
| **USERS_DN**                  | "`${USERS_OU},${DOMAIN_SUFFIX}`" if `${DOMAIN_SUFFIX}` is defined, "_ou=users,ou=system_" otherwise | The base DN for users    |
| **GROUPS_DN**                 | "`${GROUPS_OU},${DOMAIN_SUFFIX}`" if `${DOMAIN_SUFFIX}` is defined, "_ou=groups,ou=system_" otherwise | The base DN for groups |
| **INIT_WAIT**                 | `30`                  | Max time to wait for server startup                                |
| **MAX_TRIES**                 | `30`                  | Max tries to connect on server                                     |
| **WAITING_TIME**              | `2`                   | Timelaps between tries                                             |
| **ADMIN_DN**                  | `uid=admin,ou=system` | The admin user DN                                                  |
| **KEEP_DONE_SCRIPTS**         | `false`               | Set to `true` so that scripts loaded at server startup remain in the original folder (otherwise they will be moved to the `done` sub-folder by default). |
| **BACKUP_ENABLED**            | `true`                | Enable or disable auto backup                                      |
| **BACKUP_INTERVAL**           | `3600`                | Define the interval between each auto backup in seconds (30s min)  |
| **BACKUP_MAX_NB**             | `240`                 | The maximum backup files to keep                                   |
| **BACKUP_MAX_TIME**           | `14440`               | The maximum age (in minutes) of files to keep                      |


### --volume|-v

| **Variable Name**        | Container path   | Description                                                  |
|-------------------------:|:-----------------|:-------------------------------------------------------------|
| **APACHE_DS_CUSTOMDIR**  | `/root/custom`   | Contains custom files to add users or groups at server start |
| **APACHE_DS_LOGDIR**     | `/root/backups`  | Contains server backups files                                |
| **APACHE_DS_BACKUPSDIR** | `/root/logs`     | Contains server logs                                         |


## Add users/groups at server start
You can add automatically users or groups by creating initialization
files. You just have to put files into the custom volume (`/root/custom`
container path) as describe in [CUSTOM_USERS.md file](./CUSTOM_USERS.md).


## Backup/Restore
If the auto backup is enabled (by setting the environment variable **BACKUP_ENABLED** to `true`) then you can find backup files into backup folder (see volume **APACHE_DS_BACKUPSDIR**).
The logs about the backup can be found into the log folder (**APACHE_DS_LOGDIR**) in the file **backup.log**.

In order to restore a backup you have to put one backup file (only one and no more) into the custom directory (**APACHE_DS_CUSTOMDIR**) and restart the service. The backup will be restored at startup. You can find logs in the initialization log file in the log folder (**APACHE_DS_LOGDIR**).


### Admin DC
To connect to LDAP use defined **ADMIN_DN** (default: `uid=admin,ou=system`).
