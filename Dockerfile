FROM adoptopenjdk/openjdk14:debianslim-jre
LABEL maintainer bgohier

ENV APACHE_DS_VERSION=2.0.0-M19
ENV APACHE_DS_WORKDIR=/var/lib/apacheds-${APACHE_DS_VERSION}/default
ENV APACHE_DS_CUSTOMDIR=/root/custom
ENV APACHE_DS_CUSTOM_DONE=${APACHE_DS_CUSTOMDIR}/done
ENV APACHE_DS_BACKUPSDIR=/root/backups
ENV APACHE_DS_LOGDIR=/root/logs
ENV BACKUP_ENABLED=true
ENV BACKUP_INTERVAL=3600
ENV BACKUP_MAX_NB=240
ENV BACKUP_MAX_TIME=14440
ENV TLS_ENABLE=false
ENV TLS_CA_PATH=/root/certs/ca.pem
ENV TLS_CERT_PATH=/root/certs/cert.pem
ENV TLS_KEY_PATH=/root/certs/key.pem
ENV TLS_ALIAS=""

RUN mkdir -p ${APACHE_DS_CUSTOM_DONE} && chmod -R 777 ${APACHE_DS_CUSTOMDIR}
RUN mkdir -p ${APACHE_DS_BACKUPSDIR} && chmod -R 777 ${APACHE_DS_BACKUPSDIR}
RUN mkdir -p ${APACHE_DS_LOGDIR} && chmod -R 777 ${APACHE_DS_LOGDIR}

RUN apt-get update && apt-get install -y ldap-utils net-tools procps && \
  curl -o /tmp/apacheds.deb "http://archive.apache.org/dist/directory/apacheds/dist/${APACHE_DS_VERSION}/apacheds-${APACHE_DS_VERSION}-amd64.deb" && \
  chmod +x /tmp/apacheds.deb && \
  dpkg -i /tmp/apacheds.deb && \
  apt-get clean autoclean && \
  apt-get autoremove --yes && \
  rm -rf /var/lib/{apt,dpkg,cache,log}/

COPY configs/* ${APACHE_DS_WORKDIR}/conf/
RUN chown apacheds:apacheds ${APACHE_DS_WORKDIR}/conf/*

VOLUME ${APACHE_DS_CUSTOMDIR}/
VOLUME ${APACHE_DS_BACKUPSDIR}/
VOLUME ${APACHE_DS_LOGDIR}/

EXPOSE 10389 10636

COPY scripts/* /root/

RUN chmod +x /root/*.sh

ENTRYPOINT ["/root/start.sh"]
