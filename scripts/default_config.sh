#!/bin/bash
#

export LDAP_HOST=localhost
export LDAP_PORT=10389
export LDAPS_PORT=10636

DEFAULT_ADMIN_DN="uid=admin,ou=system"
export DEFAULT_ADMIN_PASSWORD="secret"
DEFAULT_DOMAIN_SUFFIX="dc=example,dc=com"
DEFAULT_USERS_OU="ou=users"
DEFAULT_GROUPS_OU="ou=groups"
DEFAULT_USERS_DN="ou=users,ou=system"
DEFAULT_GROUPS_DN="ou=groups,ou=system"
USERS_FILTER="objectClass=inetOrgPerson"
GROUPS_FILTER="objectClass=groupOfNames"

export USERS_OU="${USERS_OU=${DEFAULT_USERS_OU}}"
export GROUPS_OU="${GROUPS_OU=${DEFAULT_GROUPS_OU}}"

if [ ! -z "${DOMAIN_SUFFIX}" ] ; then
  export USERS_DN="${USERS_OU},${DOMAIN_SUFFIX}"
  export GROUPS_DN="${GROUPS_OU},${DOMAIN_SUFFIX}"
else
  export USERS_DN="${USERS_DN=${DEFAULT_USERS_DN}}"
  export GROUPS_DN="${GROUPS_DN=${DEFAULT_GROUPS_DN}}"
fi

export DOMAIN_SUFFIX="${DOMAIN_SUFFIX=${DEFAULT_DOMAIN_SUFFIX}}"

export PARTITION_ID=$(echo ${DOMAIN_SUFFIX//dc=/} | sed -e 's/,.*//g')
export DOMAIN_NAME=$(echo ${DOMAIN_SUFFIX//dc=/} | sed -e 's/,/./g')

export ADMIN_DN="${ADMIN_DN=${DEFAULT_ADMIN_DN}}"
export LDAP_ADMIN_PASSWORD="${LDAP_ADMIN_PASSWORD=${DEFAULT_ADMIN_PASSWORD}}"

encoded_entry=$(echo "dn: ${DOMAIN_SUFFIX}
objectClass: domain
objectClass: top
dc: ${PARTITION_ID}
" | base64)
export BASE64_ENTRY="$(echo ${encoded_entry} | sed -e 's/ //g' | sed -e 's/\(.\{57\}\)\(.\{39\}\)/\1\n \2/')"
