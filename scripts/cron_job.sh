#!/bin/bash
LOG=${APACHE_DS_LOGDIR}/backup.log
LAST_BACKUP=/root/lastbck
APACHE_CMD=/etc/init.d/apacheds-${APACHE_DS_VERSION}-default

while true ; do

curr_check=$(date +%s)

  if [ ! -f ${LAST_BACKUP} ] ; then
    date +%s > ${LAST_BACKUP}
  fi

  prev_bck=$(cat ${LAST_BACKUP})

  if [ $[curr_check-prev_bck] -ge ${BACKUP_INTERVAL} ] ; then
    date +%s > ${LAST_BACKUP}
    echo "Backing up at $(date)" >> ${LOG}
    cd /var/lib/apacheds-${APACHE_DS_VERSION}
    tar -czf ${APACHE_DS_BACKUPSDIR}/backup_$(date +%Y%m%d%H%M%S).tar.gz default
    echo "Done" >> ${LOG}
  fi

  if [ ${BACKUP_MAX_TIME} -gt 0 ] ; then
    find ${APACHE_DS_BACKUPSDIR} -iname "*.tar.gz" -type f -mmin +${BACKUP_MAX_TIME} | while read -r file ; do
      echo "Clear old backup ${file}" >> ${LOG}
      rm -f ${file}
    done
  fi

  if [ ${BACKUP_MAX_NB} -gt 0 ] ; then
    total=$(ls -t ${APACHE_DS_BACKUPSDIR}/*.tar.gz 2>/dev/null | wc -l)
    if [ ${total} -gt ${BACKUP_MAX_NB} ] ; then
      ls -t ${APACHE_DS_BACKUPSDIR}/*.tar.gz | tail -n $(echo $[total-BACKUP_MAX_NB]) | while read -r file ; do
        echo "Clear backup ${file}" >> ${LOG}
        rm -f ${file}
      done
    fi
  fi

  sleep 30
done

exit 0
