#!/bin/bash
#

. /root/default_config.sh

LOG_FILE=${APACHE_DS_LOGDIR}/start_$(date "+%Y%m%d").log

echo "=======================================
Start LDAP Server Log [$(date "+%Y/%m/%d %T")]" >> ${LOG_FILE}

echo "Partition: ${PARTITION_ID}" | tee -a ${LOG_FILE}
echo "Domain: ${DOMAIN_NAME}" | tee -a ${LOG_FILE}
echo "Users: ${USERS_DN}" | tee -a ${LOG_FILE}
echo "Groups: ${GROUPS_DN}" | tee -a ${LOG_FILE}

DEFAULT_USER_FILE=${APACHE_DS_WORKDIR}/conf/default_users.ldif
CONFIG_FILE=${APACHE_DS_WORKDIR}/conf/config.ldif

if [ -f ${CONFIG_FILE}.template ] ; then
  echo "$(eval echo "\"$(cat ${CONFIG_FILE}.template)\"")" > "${CONFIG_FILE}"
  rm -f ${CONFIG_FILE}.template
fi
if [ -f ${DEFAULT_USER_FILE}.template ] ; then
  echo "$(eval echo "\"$(cat ${DEFAULT_USER_FILE}.template)\"")" > "${DEFAULT_USER_FILE}"
  rm -f ${DEFAULT_USER_FILE}.template
fi


get_sub_group_pattern()
{
  ou_name_value="$1"
  ou_path_value="$2"
  echo "
dn: ${ou_path_value},${DOMAIN_SUFFIX}
objectClass: organizationalUnit
objectClass: top
ou: ${ou_name_value}


"
}

TMP_FILE="${DEFAULT_USER_FILE}.tmp"
SUB_OU_ADDED=""
add_sub_ou()
{
  list="$1"
  parent="$2"
  dbg="$3"
  [ "${dbg}" = "--debug" ] && debug=true || debug=false
  if [ ! -z "${list}" ] ; then
    ${debug} && echo "DEBUG: list=${list}"
    if echo "${list}" | grep -qe '^\(.\+,\)*[^,]\+$' ; then
      group="$(echo "$list" | sed 's/^\(\(.\+\),\)*\([^,]\+\)$/\3/g')"
      sub_group="$(echo "${list}" | sed 's/^\(\(.\+\),\)*\([^,]\+\)$/\2/g')"
      if [ ! -z "${parent}" ] ; then
        parent="${group},${parent}"
      else
        parent="${group}"
      fi
      ou_name="$(echo "${group}" | cut -d"=" -f2)"
      ${debug} && echo "DEBUG: ou_name=${ou_name}, parent=${parent}, sub_group=${sub_group}, group=${group}"
      if [ ! -z "${ou_name}" ] ; then
        if ! echo ":${SUB_OU_ADDED}:" | sed -e "s/ /:/g" | grep -qe ":${parent}:" ; then
          echo "Adding instruction to create sub OU '${ou_name}'" | tee -a ${LOG_FILE}
          content="$(get_sub_group_pattern "${ou_name}" "${parent}")"
          echo "${content}" >> "${TMP_FILE}"
          SUB_OU_ADDED="${SUB_OU_ADDED} ${parent}"
        else
          echo "Sub OU '${parent}' creation instruction already added" | tee -a ${LOG_FILE}
        fi
        add_sub_ou "${sub_group}" "${parent}" "${dbg}"
      fi
    fi
  fi
}

echo "${USERS_OU}" | grep -qe "," && add_sub_ou "$(echo "${USERS_OU}" | cut -d"," -f2-)" ""
echo "${GROUPS_OU}" | grep -qe "," && add_sub_ou "$(echo "${GROUPS_OU}" | cut -d"," -f2-)" ""

if [ ! -z "${SUB_OU_ADDED}" ] ; then
  for sub_ou in ${SUB_OU_ADDED} ; do
    echo "Sub OU instruction created for '${sub_ou}'" | tee -a ${LOG_FILE}
  done
  echo "" >> "${TMP_FILE}"
  echo "$(cat "${TMP_FILE}" ; cat "${DEFAULT_USER_FILE}")" > "${DEFAULT_USER_FILE}"
fi

TRY=1
INIT_WAIT=${INIT_WAIT:=30}
MAX_TRIES=${MAX_TRIES:=30}
WAITING_TIME=${WAITING_TIME:=2}
STARTED=false

PATTERN="\([::]\|0.0.0.0\):${LDAP_PORT}"
if [ "${TLS_ENABLE}" = "true" -a -f /root/ldap_initialized/tls ] ; then
  PATTERN="\([::]\|0.0.0.0\):${LDAPS_PORT}"
fi

PID_FILE=${APACHE_DS_WORKDIR}/run/apacheds-default.pid
APACHE_CMD=/etc/init.d/apacheds-${APACHE_DS_VERSION}-default

force_stop()
{
  ${APACHE_CMD} stop
  pid=$(ps -eaf | grep "/bin/sh ${APACHE_CMD} console" | cut -d"
" -f 1 | cut -d" " -f8)
  if [ ! -z "${pid}" ] ; then
    kill -15 ${pid}
  fi
  rm -f ${PID_FILE}
}

bck_file="$(ls -t ${APACHE_DS_CUSTOMDIR}/*.tar.gz 2>/dev/null)"
nb_bck_files=$([ -z "${bck_file}" ] && echo 0 || echo "${bck_file}" | wc -l)

if [ ${nb_bck_files} -gt 1 ] ; then
  echo "ERROR: Cannot not restore: there is multiple backup files"| tee -a ${LOG_FILE}
elif [ ${nb_bck_files} -eq 1 ] ; then
  echo "Backup file '${bck_file}' found in custom dir. Restoring it..." | tee -a ${LOG_FILE}
  cd /var/lib/apacheds-${APACHE_DS_VERSION}
  rm -rf default
  mv ${bck_file} ./
  tar -xzf $(basename "${bck_file}")
  echo "Restore done." | tee -a ${LOG_FILE}
  mkdir -p /root/ldap_initialized
  touch /root/ldap_initialized/pwd
  touch /root/ldap_initialized/ldap
  touch /root/ldap_initialized/tls
  ! ${KEEP_DONE_SCRIPTS:-false} && mv $(basename "${bck_file}") ${APACHE_DS_CUSTOM_DONE}/
fi

if netstat -eaplnt | grep -qe ${PATTERN} ; then
  force_stop
elif [ -f ${PID_FILE} ] ; then
  rm -f ${PID_FILE}
  ${APACHE_CMD} start
else
  ${APACHE_CMD} start
fi

echo "Server is starting..." | tee -a ${LOG_FILE}
sleep ${INIT_WAIT}
while [ "${STARTED}" = "false" -a ${TRY} -le ${MAX_TRIES} ] ; do
  echo "Check (${TRY}) if server is started on port ${LDAP_PORT}..." | tee -a ${LOG_FILE}
  netstat -eaplnt
  if netstat -eaplnt | grep -qe ${PATTERN} ; then
    STARTED=true
    break
  fi
  echo "Waiting ${WAITING_TIME}s..." | tee -a ${LOG_FILE}
  TRY=$((${TRY} + 1))
  sleep ${WAITING_TIME}
done

RET_CODE=0
if ${STARTED} ; then
  echo "Server is started. Begin initialization script." | tee -a ${LOG_FILE}
  /root/init_ldap.sh
  RET_CODE=$?
else
  echo "Server is not started" | tee -a ${LOG_FILE}
  RET_CODE=1
fi

if ${BACKUP_ENABLED} ; then
  echo "Enabling backup cron script"
  /root/cron_job.sh &
  CRON_ID=$!
fi

${APACHE_CMD} stop
${APACHE_CMD} console &
CONSOLE_ID=$!

trap "echo 'Stopping server...'; kill -SIGTERM ${CRON_ID} ; kill -SIGTERM ${CONSOLE_ID}" SIGINT SIGTERM

while kill -0 ${CONSOLE_ID} > /dev/null 2>&1; do
  wait
done

echo "End LDAP Server Log [$(date "+%Y/%m/%d %T")]
=======================================" >> ${LOG_FILE}

exit ${RET_CODE}
