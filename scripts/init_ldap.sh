#!/bin/bash
#

## <GLOBAL_VARIABLES> ##

INIT_FLAG_FOLDER=/root/ldap_initialized
mkdir -p ${INIT_FLAG_FOLDER}

INIT_LOG=${APACHE_DS_CUSTOMDIR}/initialization_$(date "+%Y%m%d").log

echo "=======================================
Start LDAP Initialization Log [$(date "+%Y/%m/%d %T")]" >> ${INIT_LOG}

INIT_FLAG_PWD=${INIT_FLAG_FOLDER}/pwd
INIT_FLAG_LDAP=${INIT_FLAG_FOLDER}/ldap
INIT_FLAG_TLS=${INIT_FLAG_FOLDER}/tls
TLS_ALIAS=${TLS_ALIAS:-$(hostname)}

echo "Using LDAP server: ${LDAP_HOST}:${LDAP_PORT}" | tee -a ${INIT_LOG}

CUSTOM_USERS_LST=${APACHE_DS_CUSTOMDIR}/users.lst
CUSTOM_USERS_LDIF=${APACHE_DS_CUSTOMDIR}/users.ldif
CUSTOM_GROUPS_LST=${APACHE_DS_CUSTOMDIR}/groups.lst
CUSTOM_GROUPS_LDIF=${APACHE_DS_CUSTOMDIR}/groups.ldif

if [ ! -r "${APACHE_DS_CUSTOM_DONE}" ] ; then
  mkdir -p "${APACHE_DS_CUSTOM_DONE}"
  chmod -R 777 "${APACHE_DS_CUSTOM_DONE}"
fi

USE_CUSTOM_USERS_LST=false
USE_CUSTOM_USERS_LDIF=false
USE_CUSTOM_GROUPS_LST=false
USE_CUSTOM_GROUPS_LDIF=false

INSERT_DEFAULT_USERS=${INSERT_DEFAULT_USERS:-false}

KEEP_DONE_SCRIPTS=${KEEP_DONE_SCRIPTS:-false}

## </GLOBAL_VARIABLES> ##


## <FUNCTIONS> ##

check_user_exists()
{
  user_uid="$1"
  ldapsearch -h ${LDAP_HOST} -p ${LDAP_PORT} -D "${ADMIN_DN}" -w ${LDAP_ADMIN_PASSWORD} -s sub -b "${USERS_DN}" "${USERS_FILTER}" | grep -qe '^dn: uid='${user_uid}','${USERS_DN}'$'
  echo $?
}

check_group_exists()
{
  group_name="$1"
  ldapsearch -h ${LDAP_HOST} -p ${LDAP_PORT} -D "${ADMIN_DN}" -w ${LDAP_ADMIN_PASSWORD} -s sub -b "${GROUPS_DN}" "${GROUPS_FILTER}" | grep -qe '^dn: cn='${group_name}','${GROUPS_DN}'$'
  echo $?
}

check_user_in_group_exists()
{
  user_uid="$1"
  group_name="$2"
  ldapsearch -h ${LDAP_HOST} -p ${LDAP_PORT} -D "${ADMIN_DN}" -w ${LDAP_ADMIN_PASSWORD} -s base -b "cn=${group_name},${GROUPS_DN}" "member=uid=${user_uid},${USERS_DN}" | grep -qe '^member: uid='${user_uid}','${USERS_DN}'$'
  echo $?
}

create_group()
{
  group_name="$1"
  echo "$(eval echo "\"$(cat /root/groups_template.ldif)\"")" | ldapmodify -c -a -h ${LDAP_HOST} -p ${LDAP_PORT} -D "${ADMIN_DN}" -w ${LDAP_ADMIN_PASSWORD}
}

add_user_to_group()
{
  user_uid="$1"
  group_name="$2"
  ldapmodify -h ${LDAP_HOST} -p ${LDAP_PORT} -D "${ADMIN_DN}" -w ${LDAP_ADMIN_PASSWORD} <<EOF | tee -a ${INIT_LOG}
dn: cn=${group_name},${GROUPS_DN}
add: member
member: uid=${user_uid},${USERS_DN}
EOF
}

remove_group_admin()
{
  group_name="$1"
  ldapmodify -h ${LDAP_HOST} -p ${LDAP_PORT} -D "${ADMIN_DN}" -w ${LDAP_ADMIN_PASSWORD} <<EOF | tee -a ${INIT_LOG}
dn: cn=${group_name},${GROUPS_DN}
delete: member
member: ${ADMIN_DN}
EOF
}

function get_content()
{
  local prefix="$1"
  local file="$2"
  # Remove -----(BEGIN|END) CERTIFICATE-----
  # Remove line ends
  # Split first line on nth char less than prefix size
  # Split others line on 77th char
  # Add space between all lines from the second
  local max_size=77
  local first_line_size=$[max_size-${#prefix}+1]
  sed '/^-----/d' ${file} | tr --delete '\n' | sed -e 's/.\{'${first_line_size}'\}/&\n/' | sed -e 's/.\{'${max_size}'\}/&\n/g' | sed -e '2,100s/^/ /g'
}

## </FUNCTIONS> ##


## <MAIN> ##

# <custom_users_groups_check> #
if [ -f "${CUSTOM_USERS_LST}" ] ; then
  echo "Using custom users list" | tee -a ${INIT_LOG}
  USE_CUSTOM_USERS_LST=true
fi
if [ -f "${CUSTOM_USERS_LDIF}" ] ; then
  echo "Using custom users ldif" | tee -a ${INIT_LOG}
  USE_CUSTOM_USERS_LDIF=true
fi
if [ -f "${CUSTOM_GROUPS_LST}" ] ; then
  echo "Using custom groups list" | tee -a ${INIT_LOG}
  USE_CUSTOM_GROUPS_LST=true
fi
if [ -f "${CUSTOM_GROUPS_LDIF}" ] ; then
  echo "Using custom groups ldif" | tee -a ${INIT_LOG}
  USE_CUSTOM_GROUPS_LDIF=true
fi
# </custom_users_groups_check> #

# <admin_password> #
if [ ! -f "${INIT_FLAG_PWD}" ] ; then
  ldapmodify -c -a -h ${LDAP_HOST} -p ${LDAP_PORT} -D "${ADMIN_DN}" -w ${DEFAULT_ADMIN_PASSWORD} <<EOF | tee -a ${INIT_LOG}
dn: ${ADMIN_DN}
changetype: modify
replace: userPassword
userPassword: ${LDAP_ADMIN_PASSWORD}
EOF
  if [ $? -eq 0 ] ; then
    touch "${INIT_FLAG_PWD}"
  fi
else
  echo "Password has already been changed (remove ${INIT_FLAG_PWD} to force)" | tee -a ${INIT_LOG}
fi
# </admin_password> #

# <init_ldif_file> #
if [ ! -f "${INIT_FLAG_LDAP}" -a "${INSERT_DEFAULT_USERS}" = "true" ] ; then
  echo ldapmodify -c -a -h ${LDAP_HOST} -p ${LDAP_PORT} -D "${ADMIN_DN}" -w ${LDAP_ADMIN_PASSWORD} < ${APACHE_DS_WORKDIR}/conf/default_users.ldif | tee -a ${INIT_LOG}
  ldapmodify -c -a -h ${LDAP_HOST} -p ${LDAP_PORT} -D "${ADMIN_DN}" -w ${LDAP_ADMIN_PASSWORD} < ${APACHE_DS_WORKDIR}/conf/default_users.ldif | tee -a ${INIT_LOG}
  if [ $? -eq 0 ] ; then
    touch "${INIT_FLAG_LDAP}"
  fi
elif ${INSERT_DEFAULT_USERS} ; then
  echo "LDAP has already been initialized (remove ${INIT_FLAG_LDAP} to force)" | tee -a ${INIT_LOG}
fi
# </init_ldif_file> #

# <init_tls> #
if [ ! -f "${INIT_FLAG_TLS}" -a "${TLS_ENABLE}" = "true" ] ; then
  echo "Initialize TLS" | tee -a ${INIT_LOG}
  
  echo -e "$(cat /root/certs/ca.pem)\n$(cat /root/certs/cert.pem)" > /root/certs/full.pem

  openssl pkcs12 -export -in /root/certs/full.pem -inkey /root/certs/key.pem -name ${TLS_ALIAS} -password pass:changeit -out /root/certs/server.p12

  keytool -importkeystore -srckeystore /root/certs/server.p12 -destkeystore /root/certs/keystore.jks -srcstoretype pkcs12 -deststoretype jks -srcstorepass "changeit" -deststorepass "changeit" -alias ${TLS_ALIAS}

  openssl x509 -pubkey -noout -in /root/certs/cert.pem > /root/certs/key.pub
  openssl pkcs8 -topk8 -inform PEM -outform DER -in /root/certs/key.pem -out /root/certs/key.pk8 -nocrypt

  cert_content="$(get_content "userCertificate:: " /root/certs/cert.pem)"
  pub_content="$(get_content "publicKey:: " /root/certs/key.pub)"
  key_content="$(get_content "privateKey:: " /root/certs/key.pem)"

  ldapmodify -c -x -h ${LDAP_HOST} -p ${LDAP_PORT} -D "${ADMIN_DN}" -w ${LDAP_ADMIN_PASSWORD} <<EOF | tee -a ${INIT_LOG}
dn: uid=admin,ou=system
changetype: modify
replace: userCertificate
userCertificate:: ${cert_content}
-
replace: publicKey
publicKey:: ${pub_content}
-
replace: privateKey
privateKey:: ${key_content}
EOF
  if [ $? -eq 0 ] ; then
    touch "${INIT_FLAG_TLS}"
  fi
elif ${INSERT_DEFAULT_USERS} ; then
  echo "LDAP TLS has already been initialized (remove ${INIT_FLAG_TLS} to force)" | tee -a ${INIT_LOG}
fi
# </init_tls> #

# <users_groups_dn> #
# Check if GROUPS_DN exists
ldapsearch -h ${LDAP_HOST} -p ${LDAP_PORT} -D "${ADMIN_DN}" -w ${LDAP_ADMIN_PASSWORD} -s sub -b "${GROUPS_DN}" "objectClass=organizationalUnit" | grep -qe '^dn: '${GROUPS_DN}'$'
exists=$?
if [ ${exists} -ne 0 ] ; then
  echo "Groups DN '${GROUPS_DN}' does not exist. Create it."
  ldapmodify -c -a -h ${LDAP_HOST} -p ${LDAP_PORT} -D "${ADMIN_DN}" -w ${LDAP_ADMIN_PASSWORD} <<EOF | tee -a ${INIT_LOG}
dn: ${GROUPS_DN}
objectClass: top
objectClass: organizationalUnit
ou: groups
EOF
fi

# Check if USERS_DN exists
ldapsearch -h ${LDAP_HOST} -p ${LDAP_PORT} -D "${ADMIN_DN}" -w ${LDAP_ADMIN_PASSWORD} -s sub -b "${USERS_DN}" "objectClass=organizationalUnit" | grep -qe '^dn: '${USERS_DN}'$'
exists=$?
if [ ${exists} -ne 0 ] ; then
  echo "Users DN '${USERS_DN}' does not exist. Create it."
  ldapmodify -c -a -h ${LDAP_HOST} -p ${LDAP_PORT} -D "${ADMIN_DN}" -w ${LDAP_ADMIN_PASSWORD} <<EOF | tee -a ${INIT_LOG}
dn: ${USERS_DN}
objectClass: top
objectClass: organizationalUnit
ou: users
EOF
fi
# </users_groups_dn> #

# <custom_users_groups_add> #
if ${USE_CUSTOM_USERS_LST} ; then
  echo "Adding custom users" | tee -a ${INIT_LOG}
  while read user ; do
    [ -z "${user}" ] && continue
    user_uid="$(echo "${user}" | cut -d"|" -f1)"
    user_mail="$(echo "${user}" | cut -d"|" -f2)"
    user_full_name="$(echo "${user}" | cut -d"|" -f3)"
    user_firstname="$(echo "${user}" | cut -d"|" -f4)"
    user_name="$(echo "${user}" | cut -d"|" -f5)"
    user_password="$(echo "${user}" | cut -d"|" -f6)"
    user_group="$(echo "${user}" | cut -d"|" -f7)"
    group_info="${user_group:-None}"
    echo "User[${user_uid}]: ${user_full_name} (${user_firstname} ${user_name}) <${user_mail}> - ${group_info}" | tee -a ${INIT_LOG}
    
    if [ $(check_user_exists ${user_uid}) -ne 0 ] ; then
      echo "Adding '${user_uid}'" | tee -a ${INIT_LOG}
      echo "$(eval echo "\"$(cat /root/users_template.ldif)\"")" | ldapmodify -c -a -h ${LDAP_HOST} -p ${LDAP_PORT} -D "${ADMIN_DN}" -w ${LDAP_ADMIN_PASSWORD}
      ldappasswd -h ${LDAP_HOST} -p ${LDAP_PORT} -s ${user_password} -D "${ADMIN_DN}" -w ${LDAP_ADMIN_PASSWORD} -x "uid=${user_uid},${USERS_DN}"
    else
      echo "User ID '${user_uid}' already exists in '${USERS_DN}'" | tee -a ${INIT_LOG}
    fi
    
    if [ ! -z "${user_group}" ] ; then
      created_group=false
      if [ $(check_group_exists ${user_group}) -ne 0 ] ; then
        echo "Group '${user_group}' does not exist. Creating it..." | tee -a ${INIT_LOG}
        create_group "${user_group}"
        created_group=true
      fi
      if [ $(check_user_in_group_exists ${user_uid} ${user_group}) -ne 0 ] ; then
        echo "Adding user '${user_uid}' to group '${user_group}'..." | tee -a ${INIT_LOG}
        add_user_to_group "${user_uid}" "${user_group}"
        if ${created_group} ; then
          remove_group_admin "${user_group}"
        fi
      else
        echo "User '${user_uid}' is already present in group '${user_group}'" | tee -a ${INIT_LOG}
      fi
    fi
    
  done < "${CUSTOM_USERS_LST}"
  ! ${KEEP_DONE_SCRIPTS} && mv ${CUSTOM_USERS_LST} ${APACHE_DS_CUSTOM_DONE}/
fi

if ${USE_CUSTOM_GROUPS_LST} ; then
  echo "Adding custom groups" | tee -a ${INIT_LOG}
  while read group ; do
    [ -z "${group}" ] && continue
    group_name="$(echo "${group}" | cut -d"|" -f1)"
    group_members="$(echo "${group}" | cut -d"|" -f2 | sed -e 's/,/ /g')"
    created_group=false
    if [ $(check_group_exists ${group_name}) -ne 0 ] ; then
      echo "Creating group '${group_name}'..." | tee -a ${INIT_LOG}
      create_group "${group_name}"
      created_group=true
    else
      echo "Group '' already exists" | tee -a ${INIT_LOG}
    fi
    if [ ! -z "${group_members}" ] ; then
      for user in ${group_members} ; do
        if [ $(check_user_in_group_exists ${user} ${group_name}) -ne 0 ] ; then
          echo "Adding user '${user}' to group '${group_name}'..." | tee -a ${INIT_LOG}
          add_user_to_group "${user}" "${group_name}"
        else
          echo "User '${user}' is already present in group '${group_name}'" | tee -a ${INIT_LOG}
        fi
      done
      if ${created_group} ; then
        remove_group_admin "${group_name}"
      fi
    fi
  done < "${CUSTOM_GROUPS_LST}"
  ! ${KEEP_DONE_SCRIPTS} && mv ${CUSTOM_GROUPS_LST} ${APACHE_DS_CUSTOM_DONE}/
fi

if ${USE_CUSTOM_GROUPS_LDIF} ; then
  echo "Adding custom groups LDIF" | tee -a ${INIT_LOG}
  ldapmodify -c -a -h ${LDAP_HOST} -p ${LDAP_PORT} -D "${ADMIN_DN}" -w ${LDAP_ADMIN_PASSWORD} < ${CUSTOM_GROUPS_LDIF} | tee -a ${INIT_LOG}
  ! ${KEEP_DONE_SCRIPTS} && mv ${CUSTOM_GROUPS_LDIF} ${APACHE_DS_CUSTOM_DONE}/
fi

if ${USE_CUSTOM_USERS_LDIF} ; then
  echo "Adding custom users LDIF" | tee -a ${INIT_LOG}
  ldapmodify -c -a -h ${LDAP_HOST} -p ${LDAP_PORT} -D "${ADMIN_DN}" -w ${LDAP_ADMIN_PASSWORD} < ${CUSTOM_USERS_LDIF} | tee -a ${INIT_LOG}
  ! ${KEEP_DONE_SCRIPTS} && mv ${CUSTOM_USERS_LDIF} ${APACHE_DS_CUSTOM_DONE}/
fi
# </custom_users_groups_add> #

echo "End LDAP Initialization Log [$(date "+%Y/%m/%d %T")]
=======================================" >> ${INIT_LOG}
## </MAIN> ##
exit 0
